from pycrfsuite import Trainer, Tagger
from sacred import Experiment

import numpy as np
from base import *

ex = Experiment(__file__)


@ex.config
def config():
    c1 = 0.1
    c2 = 0.01
    max_iter = 100
    scheme = "bio"

    feature_names = [
        "Bias()",
        "Lower()",
        "Suffix(2)",
        "Suffix(3)",
        "Prefix(2)",
        "Prefix(3)",
        # "StartsWithUpper()",
        "HasUpper()",
        "HasUpper(on_prev=True)",
        "HasUpper(on_next=True)",
        "HasPunction()",
        "HasPunction(on_prev=True)",
        "HasPunction(on_next=True)",
        # "HasNumber()",
        "HasNumber(on_prev=True)",
        "HasNumber(on_next=True)",
        "AllNumber()",
        "AllNumber(on_prev=True)",
        # "AllNumber(on_next=True)",
        "Shape()",
        "CompactShape()",
        "IsPlace()",
        "IsPlace(on_prev=True)",
        "IsPlace(on_next=True)",
        "IsLastName()",
        "IsLastName(on_prev=True)",
        "IsLastName(on_next=True)",
        "IsFirstName()",
        "IsFirstName(on_prev=True)",
        "IsFirstName(on_next=True)",
        "Word2VecCluster()",
        "Word2VecCluster(on_prev=True)",
        # "Word2VecCluster(on_next=True)",
        "PosTag()",
        # "PosTag(on_next=True)",
        # "PosTag(on_prev=True)",
        "PosTagShort()",
        "PosTagShort(on_prev=True)",
        # "PosTagShort(on_next=True)",
    ]


@ex.automain
def main(c1, c2, max_iter, feature_names, scheme=None):
    suffix = np.random.random()
    suffix = f"{suffix:.6f}"[2:]
    model_fn = "tmp/model.crfsuite" + suffix
    train_data_points = load_data_points("data/eng.train", scheme=scheme)
    dev_data_points = load_data_points("data/eng.testa", scheme=scheme)

    converter = Converter(train_data_points, get_features_from_names(feature_names))
    converter.annotate_itemseq(train_data_points)
    converter.annotate_itemseq(dev_data_points)

    trainer = Trainer(algorithm="lbfgs", verbose=False)
    tagger = Tagger()

    trainer.set_params({"c1": c1, "c2": c2, "max_iterations": max_iter})
    for dp in train_data_points:
        trainer.append(dp.itemseq, dp.ners)

    trainer.train(model_fn)
    tagger.open(model_fn)

    for dp in train_data_points:
        dp.ners_pred = tagger.tag(dp.itemseq)
    for dp in dev_data_points:
        dp.ners_pred = tagger.tag(dp.itemseq)

    train_f1 = evaluate_wrapper(train_data_points, fn="tmp/train" + suffix)
    dev_f1 = evaluate_wrapper(dev_data_points, fn="tmp/dev" + suffix)

    print(f"train f1: {train_f1:2.3f}")
    print(f"dev f1: {dev_f1:2.3f}")
    ex.log_scalar("train f1", train_f1)
    ex.log_scalar("dev f1", dev_f1)

    test_data_points = load_data_points("data/eng.testb", scheme=scheme)
    converter.annotate_itemseq(test_data_points)
    for dp in test_data_points:
        dp.ners_pred = tagger.tag(dp.itemseq)
    evaluate_wrapper(test_data_points, fn="crf_results.testb")

    return train_f1, dev_f1


@ex.command
def feature_ablation():
    c1 = 0.1
    c2 = 0.01
    max_iter = 100

    feature_names = [
        "Bias()",
        "Lower()",
        "Suffix(2)",
        "Suffix(3)",
        "Prefix(2)",
        "Prefix(3)",
        "StartsWithUpper()",
        "HasUpper()",
        "HasUpper(on_prev=True)",
        "HasUpper(on_next=True)",
        "HasPunction()",
        "HasPunction(on_prev=True)",
        "HasPunction(on_next=True)",
        "HasNumber()",
        "HasNumber(on_prev=True)",
        "HasNumber(on_next=True)",
        "AllNumber()",
        "AllNumber(on_prev=True)",
        "AllNumber(on_next=True)",
        "Shape()",
        "CompactShape()",
        "IsPlace()",
        "IsPlace(on_prev=True)",
        "IsPlace(on_next=True)",
        "IsLastName()",
        "IsLastName(on_prev=True)",
        "IsLastName(on_next=True)",
        "IsFirstName()",
        "IsFirstName(on_prev=True)",
        "IsFirstName(on_next=True)",
        "Word2VecCluster()",
        "Word2VecCluster(on_prev=True)",
        "Word2VecCluster(on_next=True)",
        "PosTag()",
        "PosTag(on_next=True)",
        "PosTag(on_prev=True)",
        "PosTagShort()",
        "PosTagShort(on_prev=True)",
        "PosTagShort(on_next=True)",
    ]

    o_train_f1, o_dev_f1 = main(c1, c2, max_iter, feature_names)
    for i, feature_name in enumerate(feature_names):
        new_feature_names = list(feature_names)
        new_feature_names.remove(feature_name)
        train_f1, dev_f1 = main(c1, c2, max_iter, new_feature_names)
        print(
            "[KEYLINE] {}\t{}\t{}\t{}\t{}".format(
                feature_name,
                train_f1,
                dev_f1,
                (train_f1 - o_train_f1) / o_train_f1,
                (dev_f1 - o_dev_f1) / o_dev_f1,
            )
        )
