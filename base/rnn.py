from collections import Counter
from itertools import chain

import numpy as np
import torch as tc
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pad_packed_sequence, pack_sequence, PackedSequence


class NERDataset(Dataset):
    def __init__(self, words_idx, tags_idx):
        self._words_idx = words_idx
        self._tags_idx = tags_idx

    def __getitem__(self, item):
        return self._words_idx[item], self._tags_idx[item]

    def __len__(self):
        return len(self._words_idx)

    def dataloader(self, batch_size, shuffle=False):
        def collate_fn(batch):
            batch_words_idx, batch_tags_idx = zip(*batch)

            new_indices = np.argsort([-len(x) for x in batch_tags_idx])
            batch_words_idx = [batch_words_idx[x] for x in new_indices]
            batch_tags_idx = [batch_tags_idx[x] for x in new_indices]

            words_idx_pack_seq = pack_sequence(batch_words_idx)
            tags_idx_pack_seq = pack_sequence(batch_tags_idx)

            return words_idx_pack_seq, tags_idx_pack_seq

        return DataLoader(
            self, batch_size=batch_size, shuffle=shuffle, collate_fn=collate_fn
        )


class NERModel(tc.nn.Module):
    def __init__(self, n_words, n_tags, input_dim, hidden_dim):
        super().__init__()

        self.emb = tc.nn.Embedding(n_words + 1, input_dim)
        self.rnn1 = tc.nn.LSTM(
            input_size=input_dim,
            hidden_size=hidden_dim,
            bidirectional=True,
            num_layers=1,
        )
        self.fc = tc.nn.Sequential(tc.nn.Linear(hidden_dim * 2, n_tags))

    def forward(self, x_pack):
        x_pack = PackedSequence(self.emb(x_pack.data), x_pack.batch_sizes)
        h_pack, _ = self.rnn1(x_pack)

        y_pack_data = self.fc(h_pack.data)
        y_pack = PackedSequence(y_pack_data, h_pack.batch_sizes)

        return y_pack


class NERCriterion(tc.nn.Module):
    def __init__(self):
        super().__init__()
        self.loss = tc.nn.CrossEntropyLoss(reduction="none")

    def forward(self, yp_pack, y_pack):
        y_pad, seq_lens = pad_packed_sequence(y_pack)
        yp_pad, _ = pad_packed_sequence(yp_pack)

        _loss = 0
        for y, yp, seq_len in zip(y_pad, yp_pad, seq_lens):
            _loss += self.loss(yp[:seq_len], y[:seq_len]).sum()
        _loss = _loss / tc.sum(seq_lens).to(dtype=tc.float, device=_loss.device)

        return _loss


def preprocessing(train_data_points, word_min_cnt):
    train_words = [dp.words for dp in train_data_points]
    train_tags = [dp.ners for dp in train_data_points]

    word_counter = Counter(map(lambda x: x.upper(), chain.from_iterable(train_words)))
    tag_counter = Counter(chain.from_iterable(train_tags))
    word_vocab, rword_vocab = {}, {}
    tag_vocab, rtag_vocab = {}, {}

    for idx, (tag, _) in enumerate(tag_counter.items()):
        tag_vocab[tag] = idx
        rtag_vocab[idx] = tag

    word_counter = {k: v for k, v in word_counter.items() if v > word_min_cnt}
    for idx, (word, cnt) in enumerate(word_counter.items()):
        word_vocab[word] = idx
        rword_vocab[idx] = word
    n = len(word_vocab)
    word_vocab["UNK"] = n
    rword_vocab[n] = "UNK"

    def word_to_index(word):
        return word_vocab.get(word, word_vocab.get("UNK"))

    def tag_to_index(tag):
        return tag_vocab[tag]

    def index_to_tag(idx):
        return rtag_vocab[idx]

    def convert(items, item_to_index):
        return [
            tc.LongTensor([item_to_index(item) for item in items]) for items in items
        ]

    def get_data_loader(data_points, batch_size, shuffle=False):
        words = [dp.words for dp in data_points]
        tags = [getattr(dp, "ners", []) for dp in data_points]
        words_idx = convert(words, word_to_index)
        tags_idx = convert(tags, tag_to_index)

        dataset = NERDataset(words_idx, tags_idx)
        dataloader = dataset.dataloader(batch_size, shuffle=shuffle)

        return dataloader

    return (
        get_data_loader,
        word_to_index,
        index_to_tag,
        max(rword_vocab.keys()) + 1,
        max(rtag_vocab.keys()) + 1,
    )
