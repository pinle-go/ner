import re

from utils.data_converter import tags_from_conll


class DataPoint:
    def __init__(self, words, pos_tags, chunks, ners, originals, tag_schema=None):
        self.words = words
        self.pos_tags = (
            pos_tags
            if tag_schema is None
            else tags_from_conll(pos_tags, scheme=tag_schema)
        )
        self.chunks = chunks
        self.ners = ners
        self.originals = originals

        self.tag_schema = tag_schema

    @staticmethod
    def from_conll2003(s, scheme):
        if s == "":
            return None

        words_extra = [x.split() for x in s.split("\n")]

        return DataPoint(
            [x[0] for x in words_extra],
            [x[1] for x in words_extra],
            [x[2] for x in words_extra],
            [x[3] for x in words_extra if len(x) == 4],
            [x for x in s.split("\n")],
            tag_schema=scheme
        )

    def __repr__(self):
        return str(self)

    def __str__(self):
        if len(self.ners) != len(self.words):
            return " ".join(self.words)
        else:
            result = []
            for word, ner in zip(self.words, self.ners):
                result.append(f"{word:15s}\t{ner:6s}")
            return "\n".join(result)

    def set_itemseq(self, itemseq):
        self.itemseq = itemseq

    def set_indices(self, indices):
        self.indices = indices

    def set_onehot(self, onehot):
        self.onehot = onehot


class Converter:
    def __init__(self, data_points, features):
        self.features = features

    def annotate_itemseq(self, data_points):
        for dp in data_points:
            itemseqs = []
            for i in range(len(dp.words)):
                result = [f.infer(dp, i) for f in self.features]
                itemseqs.append(result)

            dp.set_itemseq(itemseqs)


def load_data_points(fn, scheme='bio'):
    data_points = []

    with open(fn) as f:
        lines = f.read()
        sentences = re.split(r"\n\n+", lines)[1:]
        for s in sentences:
            t = DataPoint.from_conll2003(s, scheme)
            if t is not None:
                data_points.append(t)

    return data_points
