from utils.data_converter import tags_to_conll
from .data import DataPoint, Converter, load_data_points
from .features import get_features_from_names


def evaluate_wrapper(data_points, fn):
    from utils.conlleval import evaluate

    results = []
    for dp in data_points:
        result = []
        for o, ner_pred in zip(dp.originals, dp.ners_pred):
            if dp.tag_schema is not None:
                ner_pred = tags_to_conll([ner_pred])[0]
            result.append(f"{o} {ner_pred}")
        results.append("\n".join(result))
        results.append("\n")
    with open(fn, "w") as f:
        f.write("\n".join(results))
    with open(fn) as f:
        lines = f.readlines()
        pre, rec, f1 = evaluate(lines)

    return f1
