import json
import os
import re
import string

import numpy as np

_data_folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), "../data")


class FeatureBase:
    def __init__(self, on_prev=False, on_next=False):
        self.on_prev = on_prev
        self.on_next = on_next

    def h(self, feature):
        raise NotImplementedError

    def infer(self, dp, i):
        feature = self._get_feature(dp, i)

        return self.h(feature)

    def _get_feature(self, dp, i):
        if self.on_next:
            if i + 1 == len(dp.words):
                return ""
            else:
                return dp.words[i + 1]
        elif self.on_prev:
            if i - 1 >= 0:
                return dp.words[i - 1]
            else:
                return ""
        else:
            return dp.words[i]


class Bias(FeatureBase):
    def h(self, word):
        return "Bias"


class Lower(FeatureBase):
    def h(self, word):
        return f"Lower={word.lower()}"


class Suffix(FeatureBase):
    def __init__(self, k, **kwargs):
        super().__init__(**kwargs)
        self.k = k

    def h(self, word):
        return f"Suffix@{self.k}={word[-self.k:]}"


class Prefix(Suffix):
    def h(self, word):
        return f"Prefix@{self.k}={word[:self.k]}"


class StartsWithUpper(FeatureBase):
    def h(self, word):
        if word[0].islower():
            return "StartsWithUpper=0"

        return "StartsWithUpper=1"


class HasUpper(FeatureBase):
    def h(self, word):
        for char in word:
            if not char.islower():
                return "HasUpper=1"

        return "HasUpper=0"


class HasPunction(FeatureBase):
    punctuations = set(string.punctuation)

    def h(self, word):
        for c in word:
            if c in HasPunction.punctuations:
                return "HasPunction=1"
        return "HasPunction=0"


class HasNumber(FeatureBase):
    def h(self, word):
        for c in word:
            if c.isdigit():
                return "HasNumber=1"
        return "HasNumber=0"


class AllNumber(FeatureBase):
    def h(self, word):
        for c in word:
            if not c.isdigit():
                return "AllNumber=0"
        return "AllNumber=1"


class Shape(FeatureBase):
    def h(self, word):
        t = re.sub("[A-Z]", "X", word)
        t = re.sub("[a-z]", "x", t)
        t = re.sub("[0-9]", "d", t)
        return f"Shape={t}"


class CompactShape(FeatureBase):
    def h(self, word):
        t = re.sub("[A-Z]", "X", word)
        t = re.sub("[a-z]", "x", t)
        t = re.sub("[0-9]", "d", t)

        r = []
        c_curr, n_curr = "", 0

        for c in t:
            if c != c_curr:
                r.append(c_curr)
                if n_curr > 1:
                    r.append("+")
                c_curr, n_curr = c, 1
            else:
                n_curr += 1
        r = "".join(r)
        return f"CompactShape={r}"


class IsLastName(FeatureBase):
    with open(os.path.join(_data_folder, "last_names.txt")) as f:
        last_names = set([line.strip().upper() for line in f])

    def h(self, word):
        r = 1 if word.upper() in IsLastName.last_names else 0
        return f"IsLastName={r}"


class IsFirstName(FeatureBase):
    with open(os.path.join(_data_folder, "first_names.txt")) as f:
        first_names = set([line.strip().upper() for line in f])

    def h(self, word):
        r = 1 if word.upper() in IsFirstName.first_names else 0
        return f"IsFirstName={r}"


class IsPlace(FeatureBase):
    with open(os.path.join(_data_folder, "places.txt")) as f:
        places = set([line.strip().upper() for line in f])

    def h(self, word):
        r = 1 if word.upper() in IsPlace.places else 0
        return f"IsPlace={r}"


class Word2VecCluster(FeatureBase):
    with open(os.path.join(_data_folder, "glove.6B.50d.parsed.json")) as f:
        word2vec = json.load(f)

    def h(self, word):
        r = Word2VecCluster.word2vec.get(word.upper(), -1)
        return f"Word2VecCluster={r}"


class PosTag(FeatureBase):
    def h(self, pos_tag):
        return f"POS_TAG={pos_tag}"

    def _get_feature(self, dp, i):
        if self.on_next:
            if i + 1 == len(dp.words):
                return ""
            else:
                return dp.pos_tags[i + 1]
        elif self.on_prev:
            if i - 1 >= 0:
                return dp.pos_tags[i - 1]
            else:
                return ""
        else:
            return dp.pos_tags[i]


class PosTagShort(PosTag):
    def h(self, pos_tag):
        return f"POS_TAG={pos_tag[:2]}"


def get_features_from_names(feature_names):
    return [eval(name) for name in feature_names]
