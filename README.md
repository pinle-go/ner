# code for NER

To reproduce the results, **Python 3.6** is required.

0. clone this repo

```bash
$ git clone https://gitlab.com/pinle-go/ner hp-ner
$ cd hp-ner
$ mkdir tmp
```

1. install required packages

```bash
$ pip3 install -r requirements
```

2. get the crf prediction result in (crf_results.testb)

```bash
$ python3 run_crf.py
```

3. get the rnn prediction result in (rnn_results.testb)

```bash
$ python3 run_rnn.py
```
