import numpy as np
import torch as tc
from torch.nn.utils.rnn import pad_packed_sequence, pack_sequence
from sacred import Experiment


from base import load_data_points, evaluate_wrapper
from base.rnn import preprocessing, NERModel, NERCriterion

ex = Experiment(__file__)


@ex.config
def config():
    batch_size = 32
    epochs = 100
    test_per_epochs = 10
    lr = 1e-3

    input_dim = 50
    hidden_dim = 20

    word_min_cnt = 2
    use_pretrained = True


@ex.automain
def main(
    epochs,
    test_per_epochs,
    word_min_cnt,
    batch_size,
    input_dim,
    hidden_dim,
    lr,
    use_pretrained,
    _log,
    _run,
):
    train_data_points = load_data_points("data/eng.train")
    dev_data_points = load_data_points("data/eng.testa")

    get_dl, word_to, to_tag, n_words, n_tags = preprocessing(
        train_data_points, word_min_cnt
    )
    train_dataloader = get_dl(train_data_points, batch_size=batch_size)

    model = NERModel(n_words, n_tags, input_dim, hidden_dim)
    criterion = NERCriterion()
    optimizer = tc.optim.Adam(model.parameters(), lr=lr)
    device = tc.device("cuda" if tc.cuda.is_available() else "cpu")

    cnt = 0
    if use_pretrained:
        with open("data/glove.6B.50d.txt") as f:
            for line in f:
                word, vector = line.strip().split(" ", 1)
                index = word_to(word.upper())
                if index != word_to("UNK"):
                    model.emb.weight[index, :].data.copy_(
                        tc.FloatTensor([float(x) for x in vector.split()])
                    )
                    cnt += 1
    print(f"{cnt} words matched")

    _log.info(model)
    model.to(device)

    def train():
        model.train()

        losses = []
        for x_pack, y_pack in train_dataloader:
            x_pack = x_pack.to(device)
            y_pack = y_pack.to(device)
            optimizer.zero_grad()

            yp_pack = model(x_pack)
            loss = criterion(yp_pack, y_pack)
            losses.append(loss.item())

            loss.backward()
            optimizer.step()

        return np.mean(losses)

    def tag(words):
        model.eval()
        words_idx = [tc.LongTensor([word_to(word) for word in words])]
        packed_seq = pack_sequence(words_idx)
        packed_seq = packed_seq.to(device=device)

        with tc.no_grad():
            yp_packed = model(packed_seq)
            yp_pad, yp_len = pad_packed_sequence(yp_packed)
            tags_idx = yp_pad.argmax(dim=2).cpu().flatten().tolist()

        return [to_tag(tag_idx) for tag_idx in tags_idx]

    for epoch in range(epochs + 1):
        avg_loss = train()
        print(f"Epoch {epoch:3d} Training Loss: {avg_loss:3.3f}")

        if epoch % test_per_epochs == 0:
            for dp in train_data_points:
                dp.ners_pred = tag(dp.words)
            train_f1 = evaluate_wrapper(train_data_points, fn="tmp/rnn")

            for dp in dev_data_points:
                dp.ners_pred = tag(dp.words)
            dev_f1 = evaluate_wrapper(dev_data_points, fn="tmp/rnn")
            print(train_f1, dev_f1)

    test_data_points = load_data_points("data/eng.testb")
    for dp in test_data_points:
        dp.ners_pred = tag(dp.words)
    evaluate_wrapper(test_data_points, fn="rnn_results.testb")
