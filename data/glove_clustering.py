# need to download data first
# link: http://nlp.stanford.edu/data/glove.twitter.27B.zip

import os
import json

import numpy as np
from sklearn.cluster import KMeans

# %%
raw_data_fn = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "glove.6B.50d.txt"
)

word_to_index = {}
index_to_word = {}
index_to_vector = []

with open(raw_data_fn) as f:
    for line in f:
        line = line.strip()
        word, vector = line.split(" ", 1)
        vector = [float(x) for x in vector.split()]
        n = len(word_to_index)
        word_to_index[word] = n
        index_to_word[n] = word
        index_to_vector.append(vector)

# %%
clf = KMeans(n_clusters=99, random_state=99, n_jobs=-1)
x = np.array(index_to_vector)
y = clf.fit_transform(x).argmax(axis=1).tolist()

word_to_cluster = {}
for word, index in word_to_index.items():
    word_to_cluster[word.upper()] = y[index]

with open(raw_data_fn.split(".txt", 1)[0] + ".parsed.json", "w") as f:
    json.dump(word_to_cluster, f)
